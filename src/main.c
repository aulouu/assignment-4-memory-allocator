#define _GNU_SOURCE

#include <assert.h>
#include <stdio.h>
#include <sys/mman.h>

#include "mem.h"
#include "mem_internals.h"

#define TESTS_PASSED 0
#define DEFAULT_HEAP_SIZE 4096
#define HEAP_SIZE_DIVIDE_TWO 2048
#define HEAP_SIZE_MULTIPLY_TWO 8192
#define HEAP_START ((void*)0x04040000)
#define get_header(mem) \
    ((struct block_header*) (((uint8_t*) (mem)) - offsetof(struct block_header, contents)))

void debug(const char* fmt, ... );

void* map_pages(void const* addr, size_t length, int additional_flags) {
    return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , -1, 0 );
}

void test_success_malloc(){
    debug("Test 1 run...\n");

    debug("\nHeap initialization...\n", DEFAULT_HEAP_SIZE);
    void* heap = heap_init(DEFAULT_HEAP_SIZE);
    assert(heap);
    debug("\nHeap initialization success\n", DEFAULT_HEAP_SIZE);

    debug("\nMemory allocation...\n", HEAP_SIZE_DIVIDE_TWO);
    void* allocated = _malloc(HEAP_SIZE_DIVIDE_TWO);
    assert(allocated);
    debug("\nAllocation sucess\n");

    debug("\nMemory allocation...\n", HEAP_SIZE_MULTIPLY_TWO);
    allocated = _malloc(HEAP_SIZE_MULTIPLY_TWO);
    assert(allocated);
    debug("\nAllocation sucess\n");

    heap_term();
    debug("\nTest 1: passed!\n");
}

void test_one_block_free() {
    debug("\n\nTest 2 run...\n");

    debug("\nHeap initialization...\n", 0);
    void* heap = heap_init(0);
    assert(heap);
    debug("\nHeap initialization success\n", 0);

    debug("\nBlocks initialization...\n", 0);
    void* first_block = _malloc(sizeof(uint8_t));
    void* second_block = _malloc(sizeof(uint8_t));
    assert(first_block);
    assert(second_block);
    debug("\nBlocks initialization success\n", 0);

    _free(second_block);
    assert(!get_header(first_block)->is_free);
    assert(get_header(second_block)->is_free);

    heap_term();
    debug("\nTest 2: passed!\n");
}

void test_two_blocks_free() {
    debug("\n\nTest 3 run...\n");

    debug("\nHeap initialization...\n", 0);
    void* heap = heap_init(0);
    assert(heap);
    debug("\nHeap initialization success\n", 0);

    debug("\nBlocks initialization...\n", 0);
    void* first_block = _malloc(sizeof(uint8_t));
    void* second_block = _malloc(sizeof(uint8_t));
    assert(first_block);
    assert(second_block);
    debug("\nBlocks initialization success\n", 0);

    _free(first_block);
    _free(second_block);
    assert(get_header(first_block)->is_free);
    assert(get_header(second_block)->is_free);

    heap_term();
    debug("\nTest 3: passed!\n");
}

void test_same_regions_expansion() {
    debug("\n\nTest 4 run...\n");

    debug("\nHeap initialization...\n", 0);
    struct region* heap = heap_init(0);
    assert(heap);
    debug("\nHeap initialization success\n", 0);

    size_t initial_region_size = heap->size;

    debug("\nOverflow block initialization...\n", 0);
    _malloc(3 * DEFAULT_HEAP_SIZE);
    size_t expanded_region_size = heap->size;
    debug("\nBlock initialization success\n", 0);

    assert(initial_region_size < expanded_region_size);

    heap_term();
    debug("\nTest 4: passed!\n");
}

void test_different_regions_expansion() {
    debug("\n\nTest 5 run...\n");

    void* pre_allocated = map_pages(HEAP_START, 10, MAP_FIXED);
    assert(pre_allocated);
    void* allocated_filled_ptr = _malloc(10);
    assert(allocated_filled_ptr);
    assert(pre_allocated != allocated_filled_ptr);

    heap_term();
    debug("\nTest 5: passed!\n");
}

int main() {
    test_success_malloc();
    test_one_block_free();
    test_two_blocks_free();
    test_same_regions_expansion();
    test_different_regions_expansion();

    debug("\n\nAll tests passed!\n");
    return TESTS_PASSED;
}
